options(bitmapType="cairo")
img_filename="allele_freq_spectrum.png"
png(file=img_filename, width=1300, height=900)

# Read in the HWE table and compute counts and allele frequencies
file="H938.hwe";
hwe<-read.table(file,skip=1,as.is=TRUE)
names(hwe)<-c("chr","SNP.id","which.inds","a1","a2","genotype","obs.het","exp.het","HWE.pval")
counts<-sapply(hwe$genotype,function(x){as.numeric(strsplit(x,"/")[[1]])})
counts<-t(counts)
tot.counts<-rowSums(counts)
allele.counts<-(2*counts[,1]+counts[,2])

# Flip allele counts so that we are sure we always have the minor
# allele frequency
# (Note: this uses a trick based on boolean math where true/false = 1/0).
counts.maf <- allele.counts*(allele.counts<2*tot.counts-allele.counts)
# +(2*tot.counts-allele.counts)*(allele.counts<2*tot.counts-allele.counts)

# ^ The line above wasn't functional, so I commented it out.
# It's probably a mistake in Novembre's original guide.
# If you include it, the plot turns out empty.

# Set the number of individuals by looking at the sites w/ the most
# observed data
n=max(tot.counts)

# Make the plot but filter on using only sites with fully observed
# data (i.e. totcounts==n)
hist(counts.maf[tot.counts==n],xlab="Minor allele count",
ylab="# of SNPs",main="Allele frequency spectrum",breaks=n)

# Plot the expected minor allele frequency spectra for the standard
# neutral model (i.e. constant size population, all loci neutral)
# To do so we compute, Watterson's estimator of Theta
S=sum(tot.counts==n & counts.maf>0)
thetaW=S/sum(1/seq(1,2*n-1))
# Which determines the expected AFS
expectedAFS=(1/seq(1,n)+1/(n*2-seq(1,n))) * thetaW
# And then plot
lines(seq(1,n),expectedAFS,col=2)
# Note: This adds a red line displaying the expected AFS shape
# controlled to match the data w.r.t to Watterson's Theta (i.e. the total number of SNPs).

write(paste('Image created at', img_filename), stdout())
dev.off()
