options(bitmapType="cairo")
img_filename="q_plot.png"
png(file=img_filename, width=600, height=600)

# Read in the p-values
qassoc=read.table("H938_Euro_simulated.pheno.qassoc",header=TRUE)

# Produce expected p-values from the null (i.e. perfectly uniformly
# distributed).
nTests=length(qassoc$SNP)
Unif=seq(1/nTests,1-1/nTests,length=nTests)

# Sort the -log10 p-values (i.e. match on quantile)
logUnifOrder=order(-log10(Unif),decreasing=TRUE)
SNPorder=order(-log10(qassoc$P),decreasing=TRUE)

# Plot the p-values against against each other (Note: we do for only
# the top 150K SNPs to make the number of points plotted smaller)
qmax=max(-log10(qassoc$P),na.rm=TRUE)
plot(-log10(Unif[logUnifOrder][1:150e3]),-log10(qassoc$P[SNPorder][1:150e3]),pch=16,cex=0.5,xlab="-log(p)
Expected",ylab="-log(p) Observed",,ylim=c(0,qmax));

# put in a line for the expected relationship (y=x)
abline(0,1);

# Put in a line for a Bonferroni correction (0.05 / length(qassoc$SNP)
abline(h=-log10(0.05/length(qassoc$SNP)),lty=2,col="gray")

write(paste('Image created at', img_filename), stdout())
dev.off()

