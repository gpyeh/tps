# **TP 9** · PLINK – Association Studies – GWAS

Author: Arcadi Navarro | Adapted by Juan Manuel Berros

November 2015 | September 2016

## Introduction

PLINK (Purcell et al., 2009) is a free, open-source whole genome association analysis toolset, designed to perform a range of basic, large-scale analyses in a computationally efficient manner.

The focus of PLINK is purely on analysis of genotype/phenotype data, the steps right after your genotyping experiments.

Website: https://www.cog-genomics.org/plink2

## Data

Data used here are a construct from PLINK tutorial. Approximately **20,000 autosomal SNPs were randomly selected from the genotype data of 89 Asian HapMap individuals**: 45 Han Chinese from Beiging (`HCB`) and 44 Japanese from Tokio (`JPT`).

**Two phenotypes were generated**: a **quantitative trait** and a **disease trait** (affection status, coded 1=unaffected, 2=affected), based on a median split of the quantitative trait. The quantitative trait was generated as a function of three simple components:

* A random component.
* Chinese versus Japanese identity.
* A variant on chromosome 2, **rs2222162**. We have "manually" turned this variant highly significant to see a nice result of an association study. This means that we expect to find the allele in that genome position in association with the trait.

For this lesson, we will only focus on the disease phenotype. The analysis of the quantitative trait should be the same, but applying a linear regression to the data, instead of having binary data. The file `qt.phe`, has information on the value of the QT (Quantitative Trait).

```bash
less qt.phe

# First column has the individual IDs
# HCB = Hans Chinese from Beiging; JPT = Japanese from Tokio
# Third column holds the fake phenotype values
```

The next table shows the association between the variant rs2222162 and the disease phenotype:

|          | AA  | AB  | BB  |
| ---      | --- | --- | --- |
| Controls | 17  | 22  | 6   |
| Cases    | 3   | 19  | 22  |

❓ Which genotype/allele do you think is associated with the disease? How do you think you can formally test this? Do it! We are going to use R (just a couple of simple commands). Open an R terminal and type (one line at a time):

```R
controls <- c(17, 22, 6)
cases <- c(3, 19, 22)

matrix <- as.table(rbind(controls, cases))
matrix

chisq.test(matrix)
```

Was the result significant?

The following contingency table shows the joint distribution of disease and subpopulation for the associated SNP:

|          | Chinese | Japanese |
| ---      | ---     | ---      |
| Controls | 34      | 11       |
| Cases    | 11      | 33       |

❓ Which phenomenom we might be seeing here? Do you think it will be a problem afterwards?

In summary, we have a single causal variant that is associated with disease. Complicating factors are that this variant is one of ~20,000 SNPs and also that there might be some degree of confounding of the SNP-disease associations due to the subpopulation-disease association --i.e. a possibility that population stratification effects will exist.

## What are we going to do on this hands-on?

Using PLINK we will:

- Examine the file formats for input data.
- Generate basic descriptive statistics for the whole genome SNP data: allele frequencies, deal with missing data...
- Perform basic quality control filtering. This is a very important step before starting the analysis!
- Perform a basic association analysis.
- Incorporate population in a stratified whole-genome association analysis.

## What does PLINK need to work?

To start we need two classes of files, namely:

### PED file

Contains information about genotypes.

The PED file is a white-space (space or tab) delimited file. The first six columns are mandatory:

1. Family ID # Not needed in this exercise. Only in case we had trios to identify the relationships
2. Individual ID
3. Paternal ID # “0” in this case
4. Maternal ID # idem
5. Sex # (1=male; 2=female; other=unknown)
6. Phenotype # (1=control; 2=case)

Explore the `hapmap1.ped` pedigree file.

```bash
less -S hapmap1.ped
```

Genotypes (column 7 onwards) should also be white-space delimited; they can be any character (e.g. 1,2,3,4 or A,C,G,T or anything else) except 0 which is, by default, the missing genotype character. All markers should be biallelic. All SNPs (whether haploid or not) must have two alleles specified. Either both alleles should be missing (e.g. 0) or neither. No header row should be given. For example, here are two individuals typed for 3 SNPs (one row = one person):

Search for the `JPT226` individual (remember you can search inside `less` using the `/`,
typing whatever term you want to find and `ENTER`). Can you see a region of many
contiguous zeroes in this one and the following samples?

If you see a region of zeroes like this,

```
SAMPLE1 1 1 0 0 0 0 1 2
SAMPLE2 2 1 0 0 0 0 1 1
SAMPLE3 1 2 0 0 0 0 2 2
SAMPLE4 2 1 0 0 0 0 1 1
```

Recall what the `0` genotype means. What would you say is a plausible
explanation for such a "block of zeroes"?

### MAP file

By default, each line of the MAP file describes a single marker and must contain exactly 4 columns:

1. Chromosome # (1-22, X (23), Y (24) or 0 if unplaced)
2. rs number or snp identifier
3. Genetic distance (morgans) # Not needed by now, only for dealing with recombination stuff
4. Base-pair position (bp units)

Besides, the true population membership (Chinese or Japanese) is encoded in a file `pop.phe`, coded 1=CH and 2=JP in the third column of the file. Explore this file with `less` to get an idea of what it looks like.

## Hands on!

To start, let's try PLINK by converting the `.ped` file to the binary PED format `.bed`, that uses less disk space:

```bash
plink --file hapmap1 --make-bed --out hapmap
```

Everything that plink prints on screen is also stored in a log file (`.log`). Cat the log file with `cat hapmap.log` to confirm this.

❓ How many variants and how many people were found in the input file? Try grepping the word "loaded" in the log file.

PLINK writes all the output, by default, to files named `plink` plus an extension (e.g. `plink.ped`, `plink.map`, etc.). You need to specify the `--out` option to get files named the way you want. This is usually advisable, to avoid ambiguities. Proper file naming is a good practice analogous to proper tagging of test tubes in the wet lab, so strive to be tidy!

By default PLINK codes the minor allele as A1 and the major allele as A2. Thus generally, effect sizes will be given in reference to the minor allele.

### Basic descriptive statistics

It is extremely important that before we start working on our analyses, we do a quality control to our data. Within these controls, we will take care of:

#### Missing rates

In order to calculate the proportion of individuals or SNPs missing genotypes in your dataset, type:

```bash
plink --file hapmap1 --missing --out miss_stat
```

This command gives two output files:

 - `.imiss` with the number of missing genotypes per individual
 - `.lmiss` with the number of missing genotypes per locus

❓ Explore the `.imiss` file. What's the ID of the sample with higher missing
genotypes? How many genotypes were missing for them? What's the missing percentage?

You can use `sort -k` and `tail` (or `sort -rk` and `head` :wink: ) to answer this question.
Recall that `sort`'s `-k` parameter needs a column number to sort the lines by.
Could you find the `JPT267` sample with 498 missing genotypes?

❓ Now explore the `.lmiss` file. What's the **rs ID** of the SNP with the
worst genotype call rate? What's the percentage of missing genotypes at this *locus*?
Do you see the `rs7569708` ID with 18 missing genotypes out of the 89 samples?

Notice that a relative high number of missing genotypes *for an individual* might
indicate that the sample's DNA amplification (or any other downstream process)
might have been defective. This has a clear remedy: the indibvidual can be genotyped
again with a better sample.

However, a high number of missing genotypes *for a genomic position (i.e. an rs ID)*
that is consistent across samples indicates that *the region* might be hard to amplify
due to its surrounding genome sequence.

#### Brief dbSNP detour

As a scientist in genomics, you will find yourself usually in need of more
information about a given SNP. dbSNP web is a quick resource to get data.

Get information about the problematic rs ID we just found from dbSNP web.
Google "dbSNP" and you will find NCBI's service as the first result.
Input the rs ID in the "Search Entrez dbSNP for ..." field.

❓ What's the chromosome and the GRCh37 genomic position of the SNP? What
are its alleles? At the bottom of the page you will find a summary of
allele frequencies for this SNP. What's the minor allele frequency for
east asian (`EAS`) populations at this position?

#### Allele frequencies

It's very informative to filter by allele frequencies. To estimate the allele
frequency of SNPs analyzed in a given population, use:

```bash
plink --file hapmap1 --freq --out freq_stat
```

Grep the `freq_stat.frq` file for the `rs7569708` rs ID. What's the MAF for
this SNP in out 89 samples dataset? Is it similar to the MAF you found earlier
on dbSNP for east asian populations?

We can also use PLINK to calculate the MAF *per population* (that is,
separately for `CHB` and `JPT`). This helps us in spotting stratification problems.
Very diferent MAFs between populations may give false positive associations.

This time, we need to feed PLINK with the origin of each sample, that is 
in the `pop.phe` file:

```bash
plink --file hapmap1 --freq --within pop.phe --out freq_stat_pop
```

Take a look (`less`) at the `freq_stat_pop.frq.strat` file you just generated.
Understand how the data is organized: for each SNP, there are *two rows*,
one for each cluster (`CLST` column), with different MAFs.

❓ Use `awk` to find SNPs that have a MAF = 0. What does a MAF = 0 mean for a
SNP in the given cluster/population?

❓ Can you list ALL the SNPs that have MAF = 0 *only in one population* (and thus 
MAF > 0 in the other population)?

Hint: try to solve it by yourself with `awk` and `uniq -c` before peeking at the solution below.

Let's check one possible solution. It's tricky:

```bash
awk '$6 == 0 { print $2 }' freq_stat_pop.frq.strat | uniq -c | awk '$1 == 1 { print $2 }'
```

Analyse the last command by parts. First, we kept the column with the rs IDs where the MAF (field `$6`) is equal to zero. Then we counted how many times *each* rs ID appears in that list (`uniq -c`). If a SNP is listed twice after the first filter, and hence has a count of 2, it means that it has a MAF = 0 *in both populations*. So, the next thing we need to do is to keep the SNPs that appear with MAF = 0 *only once*. That is, the SNPs with a count of 1, which means they have a MAF = 0 in just one population. I reckon the solution is a bit contrived --IMHO, a nicer way of getting this done would involve a Python script.

If you then want to list only those SNPs from the original file to check the MAFs, save the list of rs IDs in a file and then `grep -f` (`-f` for file!) using that file as a list of patterns to match:

```bash
awk '$6 == 0 { print $2 }' freq_stat_pop.frq.strat | uniq -c | awk '$1 == 1 { print $2 }' > /tmp/snps.list

# Notice we use the /tmp directory to dump temporary files which we 
# do not care to keep.

less /tmp/snps.list # Explore the file: it's just a list of rs IDs

grep -f /tmp/snps.list freq_stat_pop.frq.strat
```

For each rs ID, you should see two lines: one with the MAF 0 in one population,
and the other with the MAF ≠ 0 in the other population.

You can picture how a script could be written to find the SNPs that maximize
the allele frequency difference between populations, which might incidentally
be useful as AIMs (ancestry informative markers), but that's out of the scope
of this exercise.

### Basic quality control filtering

Now we have seen some of the most important filters we can apply to our data...
Let's apply them!

#### Missing genotypes

We will first exclude individuals and then markers. Note that the input of the first command is `hapmap1`, but its output is `hapmap2`. This set of files is then the input of the second command, which then generates a third set of files named `hapmap3`. This way, we apply the filters sequentially, each time on the results of the last applied filter. We have a pipeline going on!

```bash
# Exclude *individuals* with more than 10% missing genotypes
plink --file hapmap1 --mind 0.1 --recode --out hapmap2

# Exclude *SNPs* more than 10% missing genotypes,
# i.e. only include SNPs with at least 90% genotyping rate
plink --file hapmap2 --geno 0.1 --recode --out hapmap3

# Exclude SNPs with MAF less than 0.05
plink --file hapmap3 --maf 0.05 --recode --out hapmap4

# Exclude SNPs with a p < 0.001 in the HWE test
plink --file hapmap4 --hwe 0.001 --recode --out hapmap5
```

Now we have our final filtered set of files, named `hapmap5.{ped,map}`, to work with. Check that you have generated them correctly: `ls -lh hapmap5*`.

❓ Could you tell us how many markers/individuals were removed in each step? Remember that PLINK stores its output in `.log` files (check: `ls -lh *log`). You can quickly scan all the log files generated so far with `grep`. Try: `grep 'removed' *.log` and see the results.

❓ Can you tell us how many variants are kept at the end of the process? This time, you can `grep` the log files for the pattern "pass filters".

### Basic association analysis

There are several kinds of models of association to test. The **genotypic** test provides a general test of association in the 2-by-3 table of disease-by-genotype. The **dominant** and **recessive** models are tests for the minor allele (which is the minor allele that can be found in the output of either the `--assoc` or the `--freq` commands). That is, if B is the minor allele and A is the major allele:

| Test | Comparison |
| --- | --- |
| Allelic | B versus A |
| Dominant | (BB, BA) versus AA |
| Recessive | BB versus (BA, AA) |
| Genotypic | BB versus BA versus AA |

To test for the model that better fits the data, we run:

```bash
plink --file hapmap5 --model --fisher --out assoc_model
```

Check the results with `less assoc_model.model`. You will find five rows per SNP, one for each test performed. For each test, the counts are reported, ant the p-value of the test. Give the file a quick scan: most SNPs have high p-values (i.e. they are not associated) for the assocition tests.

Check the SNP association p-value in different models (dominant, recessive, etc.) for some SNPs:

```bash
grep 'rs1257000' assoc_model.model
grep 'rs2222162' assoc_model.model
```

You can use regular expressions (`grep -E` for "extended regular expressions") to search for more than one pattern simultaneously. Separate the patterns with a pipe `|`:

```bash
grep -E 'rs1257000|rs2222162' assoc_model.model
```

❓ Why do some of the count fields report three values and some only two values? Is it related to the kind of model being tested (GENO vs. ALLELIC vs. DOM ...)?

To get a sorted list of association results that also includes a range of significance values that are **adjusted for multiple testing**, use the `--adjust` flag:

```bash
plink --file hapmap5 --assoc --adjust --out assoc2
```

Read the last output printed by PLINK and figure out the name of the last ouput file. Browse the adjusted results opening that file with `less`. Which column displays the Bonferroni correction? Use `awk` to filter the values of that column that are less than 0.05. Are there any? Change the threshold to 0.1. Do you see any results now?

### Correcting for stratification:

When the `--adjust` command is used, the log file records the **inflation factor** calculated for the genomic control analysis, and the mean chi-squared statistic (that should be 1 under the null hypothesis).

Check the inflation factor in the log file of the last command:

```bash
grep 'inflation' *.log
```

❓ Which do you think will be the maximum λ we can obtain in this analysis?

It might be of interest to directly look at the inflation factor that results from having population membership as the phenotype in a case/control analysis, just to provide extra information about the sample. That is, replacing the disease phenotype with the one in `pop.phe`, which is actually subpopulation membership:

```bash
plink --file hapmap5 --pheno pop.phe --assoc --adjust --out assoc3
```

Testing for frequency differences between Chinese and Japanese individuals, we do see some departure from the null distribution of λ = 1.

```bash
grep 'inflation' *.log
```

❓ What are the values of λ for each set of association tests?

The genomic inflation factor (λ) compares the expected vs. the observed distribution of effect sizes. The difference in the distribution (in S.D. units) would be your genomic inflation factor. The value ~1.16 would suggest that although there is not a very strong stratification, there is perhaps a hint of an increased false positive rate.

The inflation factor of ~1.7 represents the *maximum possible inflation factor* if the disease was **perfectly correlated with subpopulation membership**. So ~1.7 is the maximum λ value that could arise from the Chinese/Japanese split in the sample (this does not account for any possible within-subpopulation structure, of course, that might also increase SNP-disease false positive rates).

### Stratification analyses

The following test compares cases and controls *within subpopulations* and averages the results.

```bash
plink --file hapmap5 --mh --within pop.phe --adjust --out stratification
```

`--mh` stands for Cochran-Mantel-Haenszel test, a test based on an "average" odds ratio that controls for the potential confounding due to the cluster variable. This test performs another association test, but correcting for the stratification in the way just described. Check the results of this test with `less -S stratification.cmh.adjusted`.

- How many variants are now significantly associated to the phenotype? Consider the Bonferroni correction p-values (`BONF` column). Is the variant that we 'manually' tweaked to be associated the one that has been found with this test?

In addition, we can correct for stratification by creating population clusters. This adjusts your group of individuals to the number of clusters you indicate. We can look for the best clustering using IBS (identity by state) method, defining a number of clusters.

```bash
plink --file hapmap5 --cluster --K 2 --out clustering
```

Check the results of the clustering with K = 2:

```bash
less -S clustering.cluster1
```

You should see one line per cluster and all the individuals assigned to each cluster. Notice that the clustering method assigned all Chinese (`HCB...`) and all Japanese (`JPT...`) people to two separate groups, except for some individuals, based on genetic data alone (no input about their origin was provided to the command!). However, we did provide the `--K 2` value, because we knew in advance that there are two populations of origin in our dataset.

We can use this 'automatic' clustering for the association analysis and achieve similar results as earlier:

```bash
plink --file hapmap5 --mh --within clustering.cluster2 --adjust --out assoc_cluster
```

The `--adjust` flag produces an output list of SNPs sorted by their corrected p-values, so we'll see the most significant ones on top of the table. Explore the results in the file `assoc_cluster.cmh.adjusted`, which take population stratification into account.

- Which SNP has a significant association to the disease?
- Is this SNP the same that we found in the previous step?

Recall that we generated this dataset by "manually" tweaking the variant `rs2222162` to be significantly associated to the phenotype. As you can see, after correcting for population structure, we ended up with the only marker that should be significant, in two different ways. Yay! 😎

### Bonus

It is possible to generate a visualization of the stratification in the sample by creating a matrix of pairwise IBS distances and then using a statistical package such as R to generate a multidimensional scaling plot (also known as Principal Coordinates Analysis or PCoA).

The MDS is similar in spirit to PCA (principal *component* analysis) but **it takes dissimilarity as input**! A dissimilarity matrix shows the *distance between every possible pair of objects* --in our case, the matrix of pairwise IBS distances. In general, the MDS displays the structure of highly-dimensional distance-like data (our dissimilarity matrix) in a **lower dimensional space without too much loss of information**. The goal is to faithfully represent these distances within the lowest possible dimensional space.

This bit was extracted from the Plink online tutorial:

```bash
plink --file hapmap1 --cluster --matrix --out ibs_view
```

Check the matrix of IBS generated by PLINK: `column -t ibs_view.mibs | less -S`. You should see one row and one column per individual (so it is an 89 x 89 shaped matrix). Do you notice the diagonal of values = 1? What do they mean?

We will compute the MDS with these data as input. Open an R session by typing `R` in the console. Then, you can write some R code:

```R
# Load the file generated by PLINK
ibs_distances <- as.matrix(read.table("ibs_view.mibs"))

# Explore the first rows
head(ibs_distances, 2)

# Generate the Multidimensional Scaling analysis
# We transform the matrix of identity values to a distance matrix with (1 - M)
principal_coords <- cmdscale(as.dist(1 - ibs_distances))

# Explore the first rows of the principal coordinales generated
head(principal_coords, 5)

# Let's define the colors we will use for the plot
# It's 45 "magenta" for 45 HCB samples and 44 "black" for 44 JPT samples
colorines <- c(rep("magenta", 45), rep("black", 44))
colorines

# Draw the plot with the principal coordinates and the colors defined.
# Magenta = Chinese; Black = Japanese
plot(principal_coords, pch=19, col=colorines)
```

This plot certainly seems to suggest that at least two quite distinct clusters exist in the sample, when you group them by their differences in genotypes. Just by exploring this kind of plot, one would be in a better position to determine which approach to stratification to subsequently take.
