## TP5

enriquecimiento funcional
PCA - MDS - TSNE... en la teoriquita ejemplos de mi tesis

# source("http://bioconductor.org/biocLite.R")
# biocLite("NOISeq")

library(NOISeq)

# Vamos a utilizar el set de datos reducido de la siguiente publicación (muy conocida)
# que suele usarse para probar paquetes relacionados con RNA-Seq:
# J.C. Marioni, C.E. Mason, S.M. Mane, M. Stephens, and Y. Gilad.
# RNA-seq: an assessment of technical reproducibility and comparison with gene expression arrays.
# Genome Research, 18: 1509 - 517, 2008.

data(Marioni)

# En el experimento de Marioni, se secuenciaron muestras de hígado y riñon de un humano (masculino) dentro
# de las 6 horas post mortem.
# Hay 5 réplicas (técnicas) por téjido, y se utilizaron 2 flowcells de Illumina para secuenciar.
# El set de datos es reducido porque solo vamos a usar los cromosomas I a V.



# preparar la información:

myfactors2 = data.frame(
  Tissue = c("Kidney", "Liver", "Kidney", "Liver", "Liver", "Kidney", "Liver",
             "Kidney", "Liver", "Kidney"),
  Run = c("R1", "R1", "R1", "R1", "R1", "R1", "R1",
          "R2", "R2", "R2"),
  TissueRun = c("Kidney_R1", "Liver_R1", "Kidney_R1", "Liver_R1", "Liver_R1", "Kidney_R1", "Liver_R1",
                "Kidney_R2", "Liver_R2", "Kidney_R2"))

mydata <- readData(data = mycounts, length = mylength, gc = mygc, biotype = mybiotypes, chromosome = mychroms, factors = myfactors)


# calcular expresión diferencial con determinados parámetros:

mynoiseq = noiseq(mydata, k = 0.5, norm = "rpkm", factor = "Tissue", lc = 1, replicates = "technical")


# ¿qué cantidad de genes tienen expresión diferencial en la comparación "tejido"?
# Explorar distintos valores de significación

head(rownames(degenes(mynoiseq, q = 0.8, M = NULL)))

head(rownames(degenes(mynoiseq, q = 0.9, M = NULL)))

head(rownames(degenes(mynoiseq, q = 0.95, M = NULL)))

head(rownames(degenes(mynoiseq, q = 0.99, M = NULL)))

# visualizaciones

DE.plot(mynoiseq, q = 0.8, graphic = "expr", log.scale = TRUE)
DE.plot(mynoiseq, q = 0.99, graphic = "expr", log.scale = TRUE, col.sel = "blue")


DE_q99 <- rownames(degenes(mynoiseq, q = 0.99, M = NULL))
DE_q99_counts <- subset(mycounts, rownames(mycounts) %in% DE_q99)
DE_q99_counts.matrix <- data.matrix(DE_q99_counts)

library(RColorBrewer)
hmcol <- colorRampPalette(brewer.pal(9, "YlOrRd"))(480) # See "?brewer.pal" for more color schemes

heatmap(DE_q99_counts.matrix, col=hmcol)


# explorar otro tipo de normalización y otras comparaciones

mynoiseq.tmm = noiseq(mydata, k = 0.5, norm = "tmm", factor = "TissueRun", conditions = c("Kidney_1", "Liver_1"), lc = 0, replicates = "technical")



