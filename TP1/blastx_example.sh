#!/bin/bash
#la primera línea es denominada "shebang", y lo que hace es decirle al interprete, que es shell, cual es el programa que va a utilizar para interpretar y ejecutar este script.
#En este casi, lo hará mediante bash, donde cada linea de este código equivale a un comando en la terminal.

cd /home/nmoreyra/blast_sequences #este comando cambia la ubicación en la terminal y se ubica en la carpeta donde se encuentran los archivos que necesito.

#blastx traduce la secuencia query (que es nucleotídica) en sus seis posibles marcos de lectura (tres marcos de lecturas por hebra) 
#y compara estas secuencias traducidas contra una base de datos de proteínas.

#En el siguiente comando "-db" es el parámetro que se ingresa para indicarle al programa blastx que se ingresará el nombre del archivo de la base de datos (con su ruta, si es necesario).
#-query es el parámetro que le indice al programa cual es la secuencia de consulta
#-out es el parámetro que le dice al programa cual será el nombre del archivo (con su ruta, si es necesario).
blastx -db $1 -query $2 -out $3

#En este ejemplo, $1, $2 y $3 son variables del lenguaje bash que permiten interactuar con el usuario mediante la terminal. Por esto, este script se corriría de la siguiente manera (en la terminal):
#./blastx_ejemplo.sh uniprot_human_proteins_reviewed.fasta query.fas salida_blastx.txt
#En este caso, la primer variable ($1) tomará el nombre de la base de datos, la segunda ($2) tomará el nombre de la secuencia de consulta y $3 tomará el nombre de la salida

blastx -db $1 -query $2 -out $3 -outfmt 6 #esto es lo mismo pero les devolverá una tabla tabulada y más fácil de leer. Los nombres de las columnas no están incluídos pero pueden buscarlas en el manual de blast.


#Tengan en cuenta que en cada linea todo lo que escriban luego del simbolo # es interpresado como un comentario y no será ejecutado.







