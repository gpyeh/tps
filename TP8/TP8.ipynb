{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1000 Genomes Selection Signals\n",
    "\n",
    "To get some exposure to different approaches for detecting recent positive selection from population genetic data, we will be using the 1000 Genomes data. We will present two approaches for detecting recent positive selection. The first is an approach based on differentiation between populations, that uses the **PBS (population-branch statistic)** to detect selection that occurred in the history of a particular population. The second is an approach based on haplotype patterns - using the **iHS statistic**. We’ll suppose a particular gene is of interest (whose transcript start/stop is marked with vertical lines below)\n",
    "\n",
    "We will interact with the 1000 Genomes data using vcf (variant call format) files. These files have one line per variant, and contain information about each individual’s genotype as columns. These files are so large they are most often kept in a `gzip` compressed format, which you will recognize usually by seeing a filename that ends with `.vcf.gz`.\n",
    "\n",
    "Before anything, we need to set up a unix-based analysis environment with the appropriate programs:\n",
    "\n",
    "- `tabix`: For quickly retrieving specific lines from a `.vcf.gz` file. It creates an index of a `.vcf.gz` file and then uses the index to quickly find the lines a user requests.\n",
    "- `bcftools`: For manipulating `vcf` files. Here we use it for some basic filtering.\n",
    "- plink-1.9 (aka plink 2): Second-generation of the multi-faceted `plink` tool. It facilitates a number of common analyses in statistical genetics and population genetics. Here we use it to compute Fst which we will need to compute the PBS statistic. We assume you will run it using the command `plink`.\n",
    "- `selscan`: For computing haplotype-based statistics that are sensitive to signatures of selective sweeps (EHH, iHS, XP-EHH, nSL, for example)\n",
    "- `norm`: A program from the `selscan` package that will normalize iHS or XP-EHH values."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Preparing files that describe the 1000 Genomes individuals\n",
    "\n",
    "To get started, we will want some basic information on the individuals in the 1000 Genomes data. We also want to simplify our analysis by just focusing on individuals from the `CHB` (Han Chinese from Beijing), `YRI` (Yoruba from Ibaden, Nigerai), and `FIN` (Finnish from Finland) populations. Let’s get started..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Downloading Individual-Level Data\n",
    "\n",
    "We can download info on the 1000 Genomes phase III individuals with the following commands. The `integrated_call_samples_v3.20130502.ALL.panel` file we are obtaining contains the sample IDs and associated populations and gender info for each individual.\n",
    "\n",
    "`wget` is a command line utility to download files from an URL.\n",
    "\n",
    "```bash\n",
    "cd ~/TP8/data/\n",
    "\n",
    "# download 1000genomes_phase3 individual information\n",
    "wget http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/integrated_call_samples_v3.20130502.ALL.panel\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Setting up `.clst.txt` and `.iids` files for `plink` and `bcftools` analyses\n",
    "\n",
    "In order to extract individuals and define groups for computing selection statistics within we need to prepare a set of `.clst.txt` and and `.iids` files. You will inspect each file after they are created to see the basic structure of these files (e.g. using the `head` command to see the start of the file and tail to see the end of the file).\n",
    "\n",
    "The `.iids` files just contain a list of the individual ids we will include in our analysis. `The .clst.txt` files define a “cluster” of individuals to consider as a single unit for analysis (e.g, a cluster might represent a population). Here we create a such file for each pair of populations we will be analyzing.\n",
    "\n",
    "Here we use `awk` commands. `awk` is very useful unix-based tool for basic file manipulation.\n",
    "\n",
    "```bash\n",
    "# use awk to produce a file that has the format of a .clst.txt file, with IID | IID | CLST for CHB, YRI,\n",
    "awk 'NR > 1 { print $1,$1,$2 }' integrated_call_samples_v3.20130502.ALL.panel > 1kgv3_clst.txt\n",
    "\n",
    "# Visually inspect the top and botom of the files.\n",
    "head 1kgv3_clst.txt\n",
    "tail 1kgv3_clst.txt\n",
    "\n",
    "# Check the individuals count per population\n",
    "awk '{ print $3 }' 1kgv3_clst.txt | sort | uniq -c\n",
    "\n",
    "# Keep the populations of interest with grep -E.\n",
    "# Bring the last executed command with CTRL+P instead of copy-pasting\n",
    "# or writing it again, and add the new part:\n",
    "awk '{ print $3 }' 1kgv3_clst.txt | sort | uniq -c | grep -E \"YRI|FIN|CHB\"\n",
    "\n",
    "# And let's compute a simple sum with one line of Python\n",
    "python -c 'print(103 + 99 + 108)'\n",
    "```\n",
    "\n",
    "We will use 310 individuals from the 1kG Project."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Downloading data for a specific 3Mb genomic region\n",
    "\n",
    "Now, let’s download a 3Mb part of the the 1000 Genomes Phase3 project data to use.\n",
    "\n",
    "Notice we will use Bash variables. First we define a variable with any name we choose --here we use `URL_BASE` and `FILENAME` as variable names. Later in the same command line session, we can retrieve those values with `${URL_BASE}` or `${FILENAME}`.\n",
    "\n",
    "```bash\n",
    "URL_BASE=\"http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502\"\n",
    "FILENAME=\"ALL.chr2.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz\"\n",
    "```\n",
    "\n",
    "Let's now use those variables in a `tabix` command to download the 1kG sample genotypes in the region of interest. It's a part of the chromosome 2. We pipe the data to `bgzip` so the resulting file is gzipped.\n",
    "\n",
    "```bash\n",
    "# Download a 3MB region (chr2:108013601-111013601)\n",
    "# from the 1000 Genomes project and store the data in a gizpped VCF\n",
    "\n",
    "tabix -h \"${URL_BASE}/${FILENAME}\" 2:108013601-111013601 | bgzip -c > 1000genomes_phase3_chr2_108013601_111013601.vcf.gz\n",
    "\n",
    "# The download should complete after a while\n",
    "# Check the file size to confirm it's not an empty file:\n",
    "\n",
    "ls -lh *vcf.gz\n",
    "\n",
    "# Do you have a 14 megabytes VCF? Then you're good to go.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Filtering the big VCF\n",
    "\n",
    "We will use our list of individual ids from the `YRI`, `FIN`, and `CHB` populations to extract *just those individuals* from the large 1000 genomes VCF (which includes the 2,504 individuals). We also want to filter out indels and multi-allelic sites, to keep only bi-allelic SNPs.\n",
    "\n",
    "```bash\n",
    "# Filter out indels (-V indels), multi-allelic sites (-m 2 -M 2)\n",
    "bcftools view -v snps -m 2 -M 2 -O z 1000genomes_phase3_chr2_108013601_111013601.vcf.gz > snv_1000genomes_chr2_108013601_111013601.vcf.gz\n",
    "\n",
    "# Wait for the process to finish\n",
    "# Do you now have a 12 MB file with the SNVs (single nucleotide variants)?\n",
    "ls -lh *vcf.gz\n",
    "```\n",
    "\n",
    "Now we want to filter out sites that violate Hardy-Weinberg equilibrium. We test in each population separately. Plink will help us do this taking the VCF as input:\n",
    "\n",
    "```bash\n",
    "# When a command is too long, it can be broken into many lines\n",
    "# with the \\ at the end indicating that the command is not finished.\n",
    "plink --vcf snv_1000genomes_chr2_108013601_111013601.vcf.gz \\\n",
    "      --within 1kgv3_clst.txt \\\n",
    "      --hardy midp\\\n",
    "      --keep-cluster-names FIN \\\n",
    "      --out tmp_fin\n",
    "      \n",
    "plink --vcf snv_1000genomes_chr2_108013601_111013601.vcf.gz \\\n",
    "      --within 1kgv3_clst.txt \\\n",
    "      --hardy midp\\\n",
    "      --keep-cluster-names CHB \\\n",
    "      --out tmp_chb\n",
    "      \n",
    "plink --vcf snv_1000genomes_chr2_108013601_111013601.vcf.gz \\\n",
    "      --within 1kgv3_clst.txt \\\n",
    "      --hardy midp\\\n",
    "      --keep-cluster-names YRI \\\n",
    "      --out tmp_yri\n",
    "```\n",
    "\n",
    "Explore one the resulting tables to confirm that the p-values of the HWE test are\n",
    "on the 9th column:\n",
    "\n",
    "```bash\n",
    "column -t tmp_yri.hwe | less -S\n",
    "```\n",
    "\n",
    "We will now list the SNPs that have a significant HW disequilibrium:\n",
    "\n",
    "```bash\n",
    "# Pull out all SNPs that fail HWE (with a loose criterion)\n",
    "# in at least one population test:\n",
    "awk '$9 < 1e-5 { print $2 }' tmp_fin.hwe tmp_chb.hwe tmp_yri.hwe | sort | uniq > tmp.exclude.txt\n",
    "\n",
    "# Explore the list of SNPs\n",
    "less tmp.exclude.txt\n",
    "\n",
    "# And count the SNPs\n",
    "wc -l tmp.exclude.txt\n",
    "```\n",
    "\n",
    "How many SNPs will be excluded?\n",
    "\n",
    "Now we perform the filtering to generate a third VCF file. We use the `bcftools view` utility, which accepts an `--exclude ID=@<filename>` parameter to leave out all variants listed in a file. Finally, the parameter `-O z` means \"Output zipped\" --it asks the program for a gzipped output, so we don't have to pipe to `bgzip` like earlier.\n",
    "\n",
    "```bash\n",
    "# Filter out HW failures\n",
    "bcftools view snv_1000genomes_chr2_108013601_111013601.vcf.gz --exclude 'ID=@tmp.exclude.txt' -O z > snv_1000genomes_chr2_108013601_111013601_HWEfiltered.vcf.gz\n",
    "```\n",
    "\n",
    "Notice the common pipeline pattern we followed to this point. We had an original VCF file with all the data. Then we filtered some variants and generated a new VCF file (leaving the original untouched). Then we applied a second filter to generate another VCF (leaving the previous one untouched). In general, we filter and generate subsets of the data in sequential steps, leaving a trace of the intermediate files for later checks.\n",
    "\n",
    "NOTE: A more rigorous approach would involve a χ 2 test for the joint set of genotype counts for each sub-population (in this case a 5 degree of freedom test)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Population-differentation-based selection scan\n",
    "\n",
    "One approach to detect recent positive selection is to see if a local genomic region shows extensive differentiation between two populations (using a population differentiation statistic such as Fst or a more formal inferential methods like BayesScan not shown here). Let’s do this for the `CHB` vs `FIN` comparison for our focal region.\n",
    "\n",
    "## Computing pairwise Fst\n",
    "\n",
    "```bash\n",
    "# Compute the Fst between CHB and FIN populations\n",
    "plink --vcf snv_1000genomes_chr2_108013601_111013601_HWEfiltered.vcf.gz \\\n",
    "      --fst \\\n",
    "      --within 1kgv3_clst.txt \\\n",
    "      --out CHB_FIN_chr2_108013601_111013601_HWEfiltered \\\n",
    "      --keep-cluster-names FIN CHB\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualizing pairwise Fst values\n",
    "\n",
    "We will use some Python's libraries to read the `plink` output and plot the values:\n",
    "\n",
    "- `pandas` to read table and csv files into manageable dataframes.\n",
    "- `matplotlib` and `seaborn` to draw plots from some of the dataframe columns.\n",
    "- `numpy` for some helper functions.\n",
    "\n",
    "### Brief note about Jupyter Notebooks\n",
    "\n",
    "Up to this point, you copied the commands found in this notebook into a Linux terminal and executed them there. However, Jupyter Notebooks offer the possibility of executing Python code in its cells.\n",
    "\n",
    "From now on, you have to execute each cell of code here in the Notebook --no need to go to the terminal.\n",
    "\n",
    "In a Jupyter notebook, you can execute the cell's code hitting `CTRL+ENTER` after selecting it. With a click in the cell, you can edit the code that's inside and the re-run it with `CTRL+ENTER` again. The output is printed below the cell each time.\n",
    "\n",
    "Try it out! Click anywhere in the cell below and hit `CTRL+ENTER`. After that, add a new line with `print(\"Goodbye World\")` and execute the cell again. The new output should be shown!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Click anywhere to edit\n",
    "# Hit CTRL + ENTER to execute\n",
    "# These Python comments do not get executed!\n",
    "\n",
    "print('Hello World')\n",
    "\n",
    "# Add the new line below:\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are ready to start the practice. There will be instruction cells like this one that don't need to be executed.\n",
    "\n",
    "First, we need to load some Python libraries:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Tell Jupyter to render plots here in the notebook\n",
    "%matplotlib inline\n",
    "\n",
    "# Import the python libraries we're going to use\n",
    "import pandas as pd\n",
    "import seaborn as sns\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "ls: cannot access 'data/*.fst': No such file or directory\r\n"
     ]
    }
   ],
   "source": [
    "# List the files in data/\n",
    "# NOTE: Commands prefixed with \"!\" are run in bash, not python\n",
    "!ls data/*.fst "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now load the table with F<sub>ST</sub> tests that was generated earlier with plink. Once we load it, we sort the new dataframe (named `df`) by the F<sub>ST</sub> value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "filename = 'data/CHB_FIN_chr2_108013601_111013601_HWEfiltered.fst'\n",
    "df = pd.read_table(filename)\n",
    "\n",
    "df.sort_values(by='FST', ascending=False).head(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will now plot the Fst values (y axis) by chromosome 2 region (x axis) using `seaborn` and `matplotlib`. The code reads easily and mostly deals with the aesthetics of the plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Plot the Fst CHB vs. FIN per chromosome position\n",
    "sns.set_style('white')\n",
    "\n",
    "# Load the dataframe 'POS' and 'FST' columns into a scatter plot\n",
    "ax = df.plot.scatter(x='POS', y='FST', figsize=(15, 6),\n",
    "                     s=50, linewidth=0, color='MediumSeaGreen')\n",
    "\n",
    "# Some plot settings\n",
    "ax.set_ylim([-0.01, 1])\n",
    "ax.tick_params(length=7, pad=10)\n",
    "ax.set_yticks([0, 0.5, 1])\n",
    "\n",
    "# Draw two lines around an intersting region\n",
    "highlighted_region = (109510927, 109605828)\n",
    "\n",
    "for xval in highlighted_region:\n",
    "    ax.axvline(x=xval, linewidth=0.75, color='DimGray')\n",
    "\n",
    "# Draw an horizontal line at Fst = 0.5\n",
    "ax.axhline(y=0.5, color='HotPink', linestyle='dashed')\n",
    "    \n",
    "# Reduce chartjunk\n",
    "sns.despine(left=True, bottom=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Questions\n",
    "1. Suppose the 99.9th percentile of Fst between CHB and FIN genome-wide is 0.5. Do you see any variants in the highlighted 3Mb region that are among the most differentiated in the genome?\n",
    "2. Can you assess from this data whether selection took place just in CHB, FIN, or both?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Calculate and plot the PBS\n",
    "\n",
    "A drawback of an approach based in Fst values is that we do not know which of the two populations (or if both) have experienced selection causing the allele frequencies to differentiate. Using three populations, we can compute a statistic that isolates a single populations. Specifically we compute an estimate of how much differentiation occured on the branch leading to a particular popualtion in a three-population tree consiting of a focal population, a sister population, and an outgroup population. These PBS statistics are based on Fst, so we start by computing pairwise Fst values."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fst between CHB and YRI\n",
    "\n",
    "Back to the Linux terminal. Run this code in a terminal and then come back to the notebook:\n",
    "\n",
    "```bash\n",
    "# Fst between CHB and YRI\n",
    "plink --vcf snv_1000genomes_chr2_108013601_111013601_HWEfiltered.vcf.gz \\\n",
    "      --fst \\\n",
    "      --within 1kgv3_clst.txt \\\n",
    "      --out CHB_YRI_chr2_108013601_111013601_HWEfiltered \\\n",
    "      --keep-cluster-names CHB YRI\n",
    "\n",
    "# Fst between FIN and YRI\n",
    "plink --vcf snv_1000genomes_chr2_108013601_111013601_HWEfiltered.vcf.gz \\\n",
    "      --fst \\\n",
    "      --within 1kgv3_clst.txt \\\n",
    "      --out FIN_YRI_chr2_108013601_111013601_HWEfiltered \\\n",
    "      --keep-cluster-names FIN YRI\n",
    "```\n",
    "\n",
    "Let’s now compute the PBS statistic for the FIN and the CHB to see if we can determine which population the selection occurred in.\n",
    "\n",
    "$$T = -log(1 - F_{ST})$$\n",
    "\n",
    "$$PBS_{CHB} = \\frac{T_{CY} + T_{CF} - T_{FY}}{2}$$\n",
    "\n",
    "$$PBS_{FIN} = \\frac{T_{FY} + T_{CF} - T_{CY}}{2}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Explore the relation between Fst and T\n",
    "\n",
    "Back to Python again. Let's explore how the values of Fst between 0 and 1 map to values of the T value defined above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from math import log\n",
    "\n",
    "def T(fst):\n",
    "    return -log(1 - fst)\n",
    "\n",
    "fst_values = np.arange(0, 1, 0.01)\n",
    "t_df = pd.DataFrame({'Fst': fst_values,\n",
    "                     'T': [T(fst) for fst in fst_values]})\n",
    "\n",
    "\n",
    "sns.set_style('white')\n",
    "\n",
    "ax = t_df.plot(x='Fst', y='T', legend=False, color='MediumSeaGreen')\n",
    "ax.set_ylabel('T')\n",
    "ax.set_xticks([0, 0.5, 1])\n",
    "ax.yaxis.grid()\n",
    "\n",
    "sns.despine(left=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "!ls data/*fst"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculate T from Fst, and then the PBS\n",
    "\n",
    "We now read the Fst values computed for each pair of populations into separate dataframes, which will be stored in a Python dictionary called `frames`. A dictionary is just a list of data that can be accessed by a name or \"key\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "frames = {}\n",
    "\n",
    "frames['CHB_YRI'] = pd.read_table('data/CHB_YRI_chr2_108013601_111013601_HWEfiltered.fst')\n",
    "frames['CHB_FIN'] = pd.read_table('data/CHB_FIN_chr2_108013601_111013601_HWEfiltered.fst')\n",
    "frames['FIN_YRI'] = pd.read_table('data/FIN_YRI_chr2_108013601_111013601_HWEfiltered.fst')\n",
    "\n",
    "index_cols = ['CHR', 'POS', 'SNP']\n",
    "\n",
    "for pop_combo in frames:\n",
    "    frames[pop_combo].set_index(index_cols, inplace=True)\n",
    "    frames[pop_combo] = frames[pop_combo]['FST']  # Just keep this series\n",
    "    frames[pop_combo].name = pop_combo  # Name the series after the populations\n",
    "\n",
    "# We merge the Fst values of different population pairs in a single dataframe\n",
    "fst = pd.concat(frames.values(), axis=1).reset_index()\n",
    "\n",
    "# The function T receives a value of Fst as argument\n",
    "fst['T_CY'] = fst['CHB_YRI'].map(T)\n",
    "fst['T_FY'] = fst['FIN_YRI'].map(T)\n",
    "fst['T_CF'] = fst['CHB_FIN'].map(T)\n",
    "\n",
    "# With the T values, compute the PBS for both CHB and FIN populations\n",
    "fst['PBS_CHB'] = (fst['T_CY'] + fst['T_CF'] - fst['T_FY']) / 2\n",
    "fst['PBS_FIN'] = (fst['T_CF'] + fst['T_FY'] - fst['T_CY']) / 2\n",
    "\n",
    "# Display some the non-null values to see what the dataframe looks like\n",
    "fst.dropna().head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now have, for each SNP in the region, many values: Fst between pairs of populations, T values for single populations, and PBS values for `CHB` and `FIN`.\n",
    "\n",
    "We are ready to plot the PBS values."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plot the PBS"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "_, axes = plt.subplots(2, sharex=True, figsize=(12, 8))\n",
    "\n",
    "fst.plot.scatter(x='POS', y='PBS_CHB', ax=axes[0],\n",
    "                 s=20, linewidth=0, color='MediumSeaGreen')\n",
    "fst.plot.scatter(x='POS', y='PBS_FIN', ax=axes[1],\n",
    "                 s=20, linewidth=0, color='MediumSeaGreen')\n",
    "\n",
    "# Tweak the x limits of the plot\n",
    "axes[1].set_xlim([fst['POS'].min() - 10**5, fst['POS'].max() + 10**5])\n",
    "\n",
    "for ax in axes:\n",
    "    ax.set_ylim([-1, 3])\n",
    "    ax.set_yticks(np.arange(-1, 4))\n",
    "    ax.yaxis.grid()\n",
    "    \n",
    "    for xval in highlighted_region:\n",
    "        ax.axvline(xval, linewidth=0.75, color='DimGray')\n",
    "\n",
    "sns.despine()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Questions\n",
    "\n",
    "1. What is the major difference between the PBS values in this region for CHB vs Finland?\n",
    "2. Draw the 3 population tree of (CHB, FIN, YRI) and mark which branch is extended the most in this region.\n",
    "3. In which population would one conclude that there has been the most accelerated differentiation?\n",
    "4. (Advanced) What are some possible interpretations for when the PBS statistic becomes negative?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Calculate and plot the iHS\n",
    "\n",
    "Back to the terminal again.\n",
    "\n",
    "Now’s let turn towards looking at haplotype-based signatures of selection. We’ll focus on the CHB for this analysis, so we’ll begin by preparing a new data file with just the CHB individuals. We will filter the file to only include variants with minor allele frequency >5%. In principle the rare variants increase our power, but for efficiency’s sake we’ll exclude them here, as most of the information for these statistics comes from common variants.\n",
    "\n",
    "```bash\n",
    "# Write a file of just IIDs from CHB\n",
    "awk '($3 == \"CHB\") {print $1}' 1kgv3_clst.txt > CHB_iids.txt\n",
    "\n",
    "# Print some of its contents\n",
    "head CHB_iids.txt\n",
    "\n",
    "# Keep CHB individuals from the filtered vcf and filter out rare variants\n",
    "# (at maf < 5%) this will be used for haplotype based selection analysis.\n",
    "bcftools view -S CHB_iids.txt -q '.05 minor' -O z snv_1000genomes_chr2_108013601_111013601_HWEfiltered.vcf.gz > CHB_chr2_108013601_111013601_HWEfiltered.vcf.gz\n",
    "\n",
    "# A new VCF with CHB data should be now present:\n",
    "ls -lh *vcf.gz\n",
    "```\n",
    "\n",
    "Haplotype-based methods use recombination rates between markers to calibrate the extent of haplotype homozygosity observed. To that end, we need to set up a list of all the markers in the format of a plink `.bim` file, and interpolate a genetic map at each of those positions. Here are the steps:\n",
    "\n",
    "```bash\n",
    "# Write bim file from vcf (-H supresses the header)\n",
    "bcftools view CHB_chr2_108013601_111013601_HWEfiltered.vcf.gz -H | awk '{print $1, $3, \"0\", $2, $4, $5}' > CHB_chr2_108013601_111013601_HWEfiltered.bim\n",
    "\n",
    "# Explore the generated .bim file --it's a list of SNPs:\n",
    "column -t CHB_chr2_108013601_111013601_HWEfiltered.bim | less\n",
    "\n",
    "# Get genetic positions and interpolate genetic positions for variants\n",
    "# not present in the genetic_map\n",
    "plink --bim CHB_chr2_108013601_111013601_HWEfiltered.bim --cm-map genetic_map_chr2_combined_b37.txt 2 --make-just-bim --out chr2_108013601_111013601_HWEfiltered_gm\n",
    "\n",
    "# Format as a .map file\n",
    "awk '{print $1, $2, $3, $4}' chr2_108013601_111013601_HWEfiltered_gm.bim > chr2_108013601_111013601_HWEfiltered_gm.map\n",
    "\n",
    "# Inspect the map file - the columns are\n",
    "# <CHR> <POS> <Position in centiMorgans> <Position in physical space>\n",
    "head chr2_108013601_111013601_HWEfiltered_gm.map\n",
    "\n",
    "# You can inspect the top lines of the file formatting the output as a table.\n",
    "head chr2_108013601_111013601_HWEfiltered_gm.map | column -t\n",
    "```\n",
    "\n",
    "Now we can finally run the analysis! The `selscan` command will compute iHS values for us, and the `norm` command normalizes them to have mean 0 and standard deviation 1 (which is useful for assessing outlier values).\n",
    "\n",
    "```bash\n",
    "# compute iHS! This will take about 5 minutes, but YMMV\n",
    "selscan --ihs --vcf CHB_chr2_108013601_111013601_HWEfiltered.vcf.gz --map chr2_108013601_111013601_HWEfiltered_gm.map --out CHB_chr2_108013601_111013601_HWEfiltered\n",
    "```\n",
    "\n",
    "Normally we would normalize the iHS –but because we are only using a 3Mb region the normalization makes little sense. Here’s what the command would like if we actually ran it.\n",
    "\n",
    "```bash\n",
    "# norm --ihs --bins 20 --files CHB_chr2_108013601_111013601_HWEfiltered.ihs.out\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Plot the iHS scores\n",
    "\n",
    "Let’s make a plot of the iHS values in this region."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "!ls data/*ihs.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's read the table with the iHS values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "ihs = pd.read_table('data/CHB_chr2_108013601_111013601_HWEfiltered.ihs.out',\n",
    "                    names=['SNP', 'POS', 'FREQ', 'ihh1', 'ihh0', 'normalized_iHS'])\n",
    "ihs.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will plot those iHS values in the y axis and the chromosome position in the x axis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "ax = ihs.plot.scatter(x='POS', y='normalized_iHS', figsize=(10, 5),\n",
    "                      s=20, color='MediumSeaGreen', linewidth=0)\n",
    "\n",
    "for xval in highlighted_region:\n",
    "    ax.axvline(xval, linewidth=0.75, color='DimGray')\n",
    "\n",
    "ax.axhline(0, linewidth=0.6, linestyle='dashed', color='Dimgray')\n",
    "    \n",
    "sns.despine()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "### Questions\n",
    "\n",
    "1. Are there any provocative outliers among the iHS scores in the highlighted region?\n",
    "2. Suppose you are unconvinced that there are interesting patterns of iHS in this region - is it possible that the PBS signal in this region is real? Put another way --does positive selection in the past necessarily leave a signature in the iHS score of a population observed today?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Make a plot of allele frequencies region:\n",
    "It's also possible to make a plot representing allele frequencies in the"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "ax = ihs.plot.scatter(x='POS', y='FREQ', s=20, color='MediumSeaGreen',\n",
    "                      linewidth=0, figsize=(10, 5))\n",
    "\n",
    "for xval in highlighted_region:\n",
    "    ax.axvline(xval, color='DimGray', linewidth=0.75)\n",
    "    \n",
    "ax.set_ylim([0, 1])\n",
    "\n",
    "sns.despine()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "You can also plot a histogram of the frequencies (an allele frequency spectrum):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "_, ax = plt.subplots(1, figsize=(8, 5))\n",
    "sns.distplot(ihs['FREQ'], bins=20, ax=ax, color='MediumSeaGreen', kde=False,\n",
    "             axlabel='Allele Frequency', hist_kws={'linewidth': 0})\n",
    "\n",
    "ax.set_ylabel('# of sites')\n",
    "ax.set_xlim([0, 1])\n",
    "\n",
    "sns.despine()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Questions\n",
    "\n",
    "Non-reference alleles are typically derived (i.e. novel alleles relative to the ancestral allele) - so for simplicity's sake let's assume they are. Does the allele frequency distribution seen here seem consistent with selection? Why or why not?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# References\n",
    "\n",
    "- `tabix` : (http://www.htslib.org/doc/tabix.html)\n",
    "- `bcftools` : (https://samtools.github.io/bcftools/bcftools.html)\n",
    "- `plink` : Chang CC, Chow CC, Tellier LCAM, Vattikuti S, Purcell SM, Lee JJ (2015) Second-generation PLINK: rising to the challenge of larger and richer datasets. GigaScience, 4. (https://www.cog-genomics.org/plink2)\n",
    "- `selscan` : ZA Szpiech and RD Hernandez (2014) selscan: an efficient multi-threaded program to calculate EHH-based scans for positive selection. Molecular Biology and Evolution, 31: 2824-2827. (https://github.com/szpiech/selscan)\n",
    "- iHS: Voigt et al (2006) A Map of Recent Positive Selection in the Human Genome. PloS Genetics 4(3): e72.\n",
    "- PBS: Yi et al (2010) Sequencing of 50 Human Exomes Reveals Adaptation to High Altitude. Science 329:75-78."
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
