# TP 6 · Analizar datos de 1kG con vcftools

Genómica de Poblaciones y Enfermedades Humanas, FCEyN UBA, 2016

Juan Manuel Berros, Cristian Rohr

## Introducción

El **Proyecto 1000 Genomas (1kG)** secuenció los **genomas de 2.504 personas de 26 poblaciones** y generó una gran cantidad de información sobre la variación genética humana. **Sus datos están disponibles para uso público**. El objetivo de 1kG es encontrar la mayor parte de las variantes genéticas (SNPs, short indels y variantes estructurales) con MAF ≥ 1% en las poblaciones estudiadas.

### Formato VCF

El formato de archivo estándar para reportar variantes de una o más muestras es el **VCF (Variant Call Format)**. Su propiedad principal es que, en su versión normal, no almacena información de todas las posiciones genómicas, sino sólo de aquéllas que difieren de la referencia. Esto implica que ocupan mucho menos espacio en disco que si guardaran el genotipo presente en cada posición cromosómica a lo largo de 3,2 gigabases. El razonamiento detrás de este formato es que, de todos modos, la mayor parte de la información genotípica es redundante entre muestras, dado que dos humanos cualesquiera comparten la mayor parte de sus secuencias genómicas.

La especificación del formato puede leerse online: https://samtools.github.io/hts-specs/VCFv4.2.pdf

Un VCF se ve así:

![vcf_screenshot](./imgs/vcf_screen.png)

En general, podemos pensar al VCF como una tabla de genotipos, donde cada fila representa una variante y cada columna una muestra. Además, al principio de la tabla encontraremos columnas extra de información sobre la variante de esa línea.

Resumiremos los aspectos esenciales de un VCF:

* Es un archivo de texto plano (distinto del texto enriquecido de programas como MS Word).
* Suele almacenarse comprimido (`.vcf.gz`, le decimos “gzippeado”).
* Tiene un número indefinido de lineas con meta-información al comienzo del archivo. Cada una de estas líneas comienza con dos numerales (`##`).
* La tabla con la información genotípica tiene un header que comienza con un numeral (`#`). El header tiene 8 columnas obligatorias:
    1. `CHROM`: el cromosoma donde está la variante, o el identificador de un contig.
    2. `POS`: la posición en el cromosoma donde está la variante.
    3. `ID`: el identificador de la variante, por ej. "rs12345". Puede haber más de un identificador para la misma variante: "rs12345;rs23456". Se deja un punto (`.`) si se desconoce.
    4. `REF`: El alelo en el genoma de la referencia. Por ej: "C".
    5. `ALT`: Bases alternativas en ese locus, separadas por coma si se trata de una variante con más de dos alelos. Por ej: "G" (el alelo alternativo es una guanina), "GCC" (el alelo alternativo es una inserción de tres nucleótidos), "G,C" (hay dos alelos alternativos: G o C).
    6. `QUAL`: El phred score de los alelos especificados en `ALT`.
    7. `FILTER`: el status de los filtros aplicados sobre esa variante, usualmente aplicados en un pipeline bioinformático. Se llena con "PASS" si esa posición pasó todos los filtros, o con el nombre de los filtros que no fueron pasados en caso contrario, e.g. "qual30;missing50".
    8. `INFO`: Información adicional opcional, en campos separados por punto y coma, siguiendo el formato `<atributo>=<valor>`. Ejemplo: "DP=154;AA=G;NS=15".

La columna `FORMAT` a continuación especifica el formato que tienen las columnas de genotipos. Usualmente, encontramos algo como `GT:GQ:DP:HQ`, donde `GT` es el genotipo, `GQ` la calidad (phred) del genotipo, `DP` la cantidad de reads mapeados en esa posición y `HQ` la calidad de los haplotipos (phred scores). Una definición de cada campo se encuentra usualmente en los meta-datos de las primeras líneas del VCF.

El resto de los campos en la fila lleva información sobre el genotipo encontrado en cada muestra para la variante y datos asociados a ese genotipo específico. El formato de la información genotípica sigue el patrón descripto en el campo `FORMAT`. Por ejemplo: `0|1:48:15:30,30` indica un genotipo heterocigota (`0|1`) de calidad `48`, leído por `15` reads, cuyos haplotipos se describen con phred score `30`.

![vcf_detail](./imgs/vcf_detail.png)

Los genotipos son descriptos con números, no bases:

* `0` es el alelo de la referencia,
* `1`, `2`, `3`... son los alelos alternativos, en el orden en que son mencionados en el campo `ALT`.

Así pues, un genotipo `0/1` significa "heterocigoto con un alelo igual a la referencia y un alelo alternativo". Si `REF` es A y `ALT` es `G,C` (suponiendo una variante trialélica, con dos alelos alternativos), `0/1` significa el genotipo A/G y `0/2` significa el genotipo A/C.

* Los genotipos separados por la barra `/` no están en fase (no se sabe a qué cromosoma pertenece cada alelo), mientras que los separados por un pipe `|` sí están en fase.

Recuerde que las calidades reportadas para genotipo y haplotipos (`GQ` y `HQ`) son valores de phred score:

![phred_scores.png](./imgs/phred_scores.png)

### VCFtools

`vcftools` es un programa de línea de comandos que incluye numerosas herramientas. Toma como input un archivo VCF (gzippeado o no) y puede generar diversas estadísticas en base a la información de variantes y muestras encontrada en el archivo.

El manual da una descripción de lo que hace cada comando: https://vcftools.github.io/man_latest.html

También se pueden leer las *man-pages* (por *manual pages*, “páginas del manual”) de manera local, en una terminal. Inténtelo:

```bash
man vcftools
```

💡 TIP: Cuando usa `less` o lee las man-pages, puede buscar un término usando la barra (`/`) y escribiendo a continuación lo que quiere buscar, seguido de ENTER. Por ejemplo: `/hardy` y ENTER busca el término "hardy". Con `n` y `p` se puede ir al resultado siguiente (**n**ext) y al anterior (**p**revious).

Puede salir de las man-pages con la tecla `q`, al igual que cuando usa `less` para leer el contenido de un archivo.

## Ejercicios

### Descargar el VCF de 1000 Genomas para una región específica

Nos interesa descargar datos de las muestras de 1kG para el gen *CETP* en el cromosoma 16. En primer lugar, usaremos `tabix` para descargar los datos de los servidores ftp en la región que nos interesa (`chr16:56995835-57017756`) y guardaremos el output en un archivo llamado `genotypes.vcf`.

💡 TIP: Intente tipear las líneas de código en la terminal, en lugar de copiarlas y pegarlas. Al principio será incómodo y se equivocará bastante, pero es parte del entrenamiento para recordar los comandos y le ayudará a desarrollar memoria muscular en los dedos.

Entre en el directorio del TP:

```bash
cd ~/TP6b  # Confirme que es el path correcto hasta el directorio
```

Cree un subdirectorio resultados y entre en él:

```bash
mkdir -p resultados && cd resultados
```

Confirme que está en el directorio `resultados` (`pwd` es **p**rint **w**orking **d**irectory):

```bash
pwd
```

La aplicación `tabix` es especialmente útil porque no descarga todos los datos de 1kG, sino solamente los de las regiones cromosómicas que el usuario especifica. El comando siguiente debería tardar alrededor de un minuto para descargar la región de interés, aunque su millaje puede variar.

```bash
tabix -fh http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr16.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz 16:56995762-57017757 > genotypes.vcf
```

Si el comando anterior tarda mucho en terminar la descarga, para no esperar podemos cancelarlo con `CTRL+C` y directamente copiar el archivo desde la carpeta del TP:

```bash
cp ../genotypes.vcf .
```

Analice el comando anterior: se copia (`cp` = copy) el archivo `genotypes.vcf`, localizado en el directorio padre del actual (`..`), al directorio actual (`.`).

Explore el archivo `genotypes.vcf`, que contiene las variantes de **todas las muestras de 1kG en la región descargada**.

```bash
less -S genotypes.vcf
```

El parámetro `-S` en `less` hace que las líneas más largas que la ventana estén ocultas, pero puede verlas con la flecha →.

Las primeras líneas de los archivos vcf son comentarios previos a los datos genotípicos. Sabemos que en este tipo de archivo las líneas de comentarios comienzan con dos numerales, `##`. Filtremos estas líneas con `grep -v`. La opción `-v`, de in<b>v</b>ert-match, hace que `grep` deje afuera las líneas donde matcheó el patrón buscado. El caret `^` significa “comienzo de la línea”, de modo que `^##` significa "líneas que comienzan con ##":

```bash
grep -v '^##' genotypes.vcf | less -S
```

En el archivo vcf encontrará la lista de posiciones en el cromosoma 16 donde se observaron variantes y, hacia la derecha, las columnas con cada genotipo por muestra de 1kG.

Cierre `less` con la tecla `q`.

### Describir Ts vs. Tv y las frecuencias alélicas

Ahora calcularemos un resumen de las transiciones y transversiones encontradas en las muestras de 1kG:

```bash
vcftools --vcf genotypes.vcf --TsTv-summary --out ratio
```

Explore el resultado:

💡 TIP: Recuerde que no necesita escribir el nombre completo de los archivos existentes; alcanza con tipear las primeras letras y apretar la tecla `TAB` para autocompletar. Si hay más de una opción, use `TAB` varias veces para seleccionarla y luego `ENTER`.

```bash
less ratio.TsTv.summary
```

* ¿Cuántas transiciones y cuántas transversiones se observaron en total en esta región?

Calculemos ahora las frecuencias alélicas:

```bash
vcftools --vcf genotypes.vcf --freq --out frequencies
```

Explore el archivo de resultados `frequencies.frq` con `less`. Se lista la frecuencia de cada alelo en cada posición con variantes.

* ¿Puede encontrar alguna variante que afecte a más de un nucleótido?

* Recordando que el número total de muestras del proyecto 1kG es de 2.504, ¿puede deducir qué es el número 5.008 que aparece bajo la columna `N_CHR` en todas las filas?

* También teniendo en cuenta este número, note que entre los alelos de menor frecuencia (en la última columna) hay una frecuencia particular que se repite mucho. ¿Qué valor es?

* Teniendo en cuenta el número de muestras de 1kG mencionado antes: ¿qué implica que muchas de las variantes tengan un alelo alternativo de frecuencia 0,00019968 y muchas otras 0,00039936? ¿Entiende cómo se llegó a esos dos números?

💡 TIP: En este punto puede serle útil una calculadora.Lubuntu trae una calculadora; puede encontrarla buscando en el menú de abajo, en "Accesories".

★ PROTIP: Si se siente envalentonadx con la línea de comandos, en lugar de la calculadora tipee `ipython` en la terminal y allí también podrá realizar operaciones aritméticas sencillas. Escríbalas (ejemplos: `1 + 1`, `2/(2 * 2504)`, `1/(2 * 2504)`) y apriete `ENTER` tras cada una. Puede salir de IPython escribiendo `exit()` o con `CTRL+D`

### Calcular la diversidad nucleotídica (π) por locus

Ahora calculemos la diversidad nucleotídica por sitio:

```bash
vcftools --vcf genotypes.vcf --site-pi --out nuc_div
```

Explore los resultados:

```bash
less nuc_div.sites.pi
```

¿Nota que los headers (`CHROM POS PI`) están corridos respecto de sus columnas respectivas? Utilice la herramienta `column` para mostrar los datos tabulados (opción `-t`):

```bash
column -t nuc_div.sites.pi
```

El output de cualquier programa puede ser leído con `less`; sólo es necesario redirigirlo con el *pipe* `|`:

```bash
column -t nuc_div.sites.pi  | less
```

Recuerde que `|` sirve para redirigir el output de un comando y utilizarlo como input de otro comando, en lugar de imprimirlo en pantalla.

Imprima sólo las primeras 15 líneas del archivo de resultados, de nuevo en formato tabulado:

```bash
column -t nuc_div.sites.pi | head -n 15
```

Ahora intentaremos ordenar estos resultados en forma descendiente e imprimir las **15 variantes con mayor diversidad nucleotídica**. El comando `sort` ordena las líneas según el valor encontrado en alguno de los campos. En este caso, nos interesa ordenar por la tercera columna, que es el valor de PI, con el parámetro `-k 3`:

```bash
column -t nuc_div.sites.pi | sort -k 3
```

¿Observa que los resultados están ordenados de manera *ascendente*? Los 15 sitios de mayor diversidad nucleotídica serán las últimas filas de la tabla, que podemos imprimir con el comando `tail`:

```bash
column -t nuc_div.sites.pi | sort -k 3 | tail -n 15
```

Una manera más prolija de lograr el mismo resultado sería ordenar la tabla de manera *descendente* por los valores de PI y quedarnos con las primeras 15 líneas. Puede invertir el orden con `sort -r`, por “reverse”:

```bash
column -t nuc_div.sites.pi | sort -r -k3 | head -n15
```

💡 TIP: Note que puede omitir el espacio entre el parámetro y su valor: `sort -k` es igual a `sort -k 3`. Además, un comando puede recibir muchos parámetros juntos: `sort -r -k3` equivale a `sort -rk3`. Pero cuidado: los parámetros que además requieren un valor específico, como `-k`, deben recibirlo a continuación. Ej: `sort -rk3` o `sort -k3r` funcionan, pero no `sort -kr3` porque la `k` y el `3` no están juntos.

```bash
column -t nuc_div.sites.pi | sort -rk3 | head -n15
```

* ¿En qué posición del cromosoma 16 está la variante del gen *CEPT* con mayor diversidad nucleotídica en las muestras de 1000 Genomas? ¿Qué valor de π tiene?

### Calcular la heterocigosidad de cada individuo

A continuación calcularemos la heterocigosidad para cada individuo. Nos interesa *f*, el coeficiente de endogamia (*coefficient of inbreeding*):

```bash
vcftools --vcf genotypes.vcf --het --out heterozygosity
```

Explore los resultados en el archivo `heterozygosity.het`: debería haber una línea por individuo.

¿Cuál es el ID y el valor de *f* del primer individuo de la lista? ¿Y del último?

💡 TIP: En `less`, puede ir al final del archivo con la`G` (mayúscula, o sea: `SHIFT + g`) y regresar al comienzo del archivo con la `g` minúscula.

Confirme que hay tantas líneas como individuos contando las líneas del archivo. Esperamos 2.504 líneas, porque ésa es la cantidad de muestras de 1kG. `wc` sirve para contar caracteres, palabras y líneas:

```bash
whatis wc
```

Con el parámetro `-l`, `wc` le permite contar líneas en un archivo:

```bash
wc -l heterozygosity.het 
```

* ¿Cuántas líneas cuenta? ¿Por qué no da exactamente 2.504?

La respuesta es sencilla. Chequee el comienzo del archivo para obtener una pista.

```bash
head heterozygosity.het
```

Ahora intentaremos filtrar los 5 individuos con mayor coeficiente de endogamia y luego los 5 individuos con menor coeficiente de endogamia. Comencemos ordenando las muestras según ese valor, que está en la quinta columna del archivo. El comando `sort` ordena numéricamente con el parámetro `-n`:

```bash
column -t heterozygosity.het | sort -nk5
```

Invertimos el orden con `-r`:

```bash
column -t heterozygosity.het | sort -nrk5
```

Conservamos sólo las primeras 5 líneas en cada caso:

```bash
column -t heterozygosity.het | sort -nrk5 | head -n 15
column -t heterozygosity.het | sort -nk5 | head -n 15
```

¿Cuál es el menor valor de *f* observado? ¿Y el mayor?

### Detectar variantes que no están en equilibrio Hardy-Weinberg

A continuación reportaremos el p-value resultante de un test de equilibrio Hardy-Weinberg (HWE) en cada sitio (Wigginton, Cutler y Abecasis (2005)).

```bash
vcftools --vcf genotypes.vcf --hardy --out hardy
```

El resultado está en el archivo `hardy.hwe`: explórelo con `less -S`.

Debería ver el número de homocigotos y heterocigotos observados vs. esperados, valor de χ² y p-values del test. Nos interesa la columna `P_HWE`: valores significativos (`p < 0.05`) implicarían que la población no está en HWE para esa variante. Filtremos con `awk` los resultados significativos:

```bash
awk '$6 < 0.05' hardy.hwe | less -S
```

En este comando, `awk` sólo conservó las líneas en las que el 6to campo (`$6`) es menor a 0,05. Compruebe si funcionó correctamente: ¿ve valores mayores a 0,05 en la sexta columna?

Repita el paso anterior, ahora conservando valores menores a 0,001 únicamente, y explore el resultado. ¿Funciona el filtro? Por último, imprima las líneas en las que la sexta columna tenga valores *mayores* a 0,05, es decir, variantes que parecen estar en equilibrio HW.

Ahora que entiende el funcionamiento de `awk`, puede contar cuántos marcadores pasan el filtro de 0,05 como umbral de p-value para el test de HWE:

```bash
awk '$6 < 0.05' hardy.hwe | wc -l
```

¿Cuántos marcadores hay en total en el archivo?

```bash
wc -l hardy.hwe
```

¿Qué porcentaje aproximado de marcadores no pasaron el test HWE? Haga el cálculo con unas líneas de Python (no escriba la parte de cada línea que dice “In [n]:”, sólo lo que le sigue):

```
ipython

...

In [1]: ratio = 129/867
In [2]: ratio
In [3]: print('El ratio es', ratio)
In [4]: percentage = round(100 * ratio, 2)
In [5]: percentage
In [6]: print('El porcentaje es', percentage, '%')
In [7]: exit()

```

### Desequilibrio de ligamiento

Ahora calcularemos el r^2, D y D’, estadísticos tradicionales de desequilibrio de ligamiento, usualmente reportados en la literatura de genética de poblaciones. El siguiente comando asume que el vcf utilizado de input tiene haplotipos en fase (los vcf de 1kG están en fase, de modo que no tendremos problemas).

```bash
vcftools --vcf genotypes.vcf --hap-r2 --out LDstats
```

Espere que el comando termine. Explore el archivo de resultados:

```bash
column -t LDstats.hap.ld | less
```

Hay valores muy bajos de r<sup>2</sup>. Intente filtrar los valores muy bajos con `awk`, siguiendo la lógica que utilizó en el ejercicio anterior. Imprima en pantalla las líneas donde r^2 es mayor a 0,75. ¿Cuántos pares de variantes se relacionan con un r^2 superior a ese umbral?

Otra manera de quedarse con los resultados de r^2 que superan un umbral es provista por `vcftools` mismo. Investigue las man-pages del comando `vcftools` corriendo:

```bash
man vcftools
```

Busque ayuda bajo el título `OUTPUT LD STATISTICS`: lea en qué consiste la opción `--min-r2 <float>` (“float” significa “floating point number”, un formato de número con decimales). Puede cerrar las man-pages con la tecla `q`.

Corra de nuevo el comando utilizando la opción que acaba de leer, de modo de quedarse únicamente con los pares de posiciones correlacionados con un r^2 > 0,8:

```bash
vcftools --vcf genotypes.vcf --hap-r2 --min-r2 0.8 --out LDstats

# Espere a que el comando termine...

column -t LDstats.hap.ld | less
```

Ahora muestre únicamente los pares de posiciones relacionados con un r^2 = 1 y además con un D' = 1. `awk` acepta más de una condición para filtrar.

```bash
awk '$5 == 1 && $7 == 1' LDstats.hap.ld | column -t | less
```

El operador `&&` significa 'AND' y se cumple si ambas condiciones son verdaderas. El operador `==` compara dos valores. Observe el resultado: ¿nota que sólo están las líneas donde r^2 y D' son iguales a 1?

Por otro lado, el operador `||` significa 'OR' y se cumple si al menos una condición es satisfecha. ¿Puede filtrar los pares de variantes que tienen o bien un r^2 = 1 o bien un D' = 1? El comando será bastante similar al anterior.

¿Puede observar entre los resultados algunas variantes con un D' = 1, pero con valores de r^2 &lt; 1? Escriba un nuevo comando de `awk` para imprimir únicamente con esas líneas.

Finalmente, escriba un nuevo comando de `awk` que sólo imprima las líneas con un D' &lt; 1 y un r^2 = 1. ¿Cuántas variantes cumplen este requisito?

### Long Run of Homozygosity

Intentaremos identificar tramos largos de homocigosidad (LRH), que pueden interpretarse como huellas genómicas de selección natural o como resultado de endogamia. Corra el siguiente comando y lea atentamente el error:

```bash
vcftools --vcf genotypes.vcf --LROH --out lrh
```

¿Entiende de qué se queja el programa? Intentemos arreglarlo agregando el parámetro que nos pide, dado que sabemos que nuestros datos son del cromosoma 16:

```bash
vcftools --vcf genotypes.vcf --LROH --out lrh --chr 16
column -t lrh.LROH | less
```

Ordene los resultados con sort por los valores de la columna `N_VARIANTS`.

```bash
column -t lrh.LROH | sort -nrk 4 | head
```

* ¿Cuántas variantes contiene el mayor tramo de homocigosidad encontrado en esta región del genoma?

### Calcular la D de Tajima 

Calcule el estadístico D de Tajima en ventanas de 1000 pb:

```bash
vcftools --vcf genotypes.vcf --TajimaD 1000 --out tajima
less tajima.Tajima.D
```

Recuerde que valores `D < 0` pueden interpretarse como la huella genómica de un sweep selectivo o de una expansión reciente de la población.

![tajimas_D](./imgs/tajimas_D.png)

Note que el valor es reportado no por variante, sino en ventanas de 1000 pb, cuyos comienzos se especifican en la columna `BIN_START`.

* ¿La región, en general, exhibe valores negativos o positivos de D de Tajima?

Ordene las filas por el valor del estadístico utilizando `sort`. Dado que la tabla incluye valores negativos y con decimales, el parámetro `sort -g` (“general numeric sort”) funciona mejor que el `-n`:

```bash
column -t tajima.Tajima.D | sort -gk4
```

### Calcular el F<sub>ST</sub> entre poblaciones

`vcftools` también puede calcular el índice de fijación F<sub>ST</sub> entre diferentes poblaciones. El índice F<sub>ST</sub> es un estadístico calculado de acuerdo a Weir y Cockerham (1984). El usuario debe especificar archivos de texto con las listas de individuos (uno por línea) que son miembros de cada población, utilizando los mismos IDs de individuo que se ven en el archivo vcf. El comando funcionará con múltiples poblaciones si se le pasan varios argumentos `--weir-fst-pop`.

El siguiente ejemplo muestra cómo calcular el F<sub>ST</sub> entre los YRI y los JPT de 1kG. El comando debería fallar:

```bash
vcftools --vcf genotypes.vcf --weir-fst-pop YRI.samples --weir-fst-pop JPT.samples --out YRI_vs_JPT
```

Lea atentamente el mensaje de error: el archivo `YRI.samples` no fue encontrado. El comando requiere que existan los archivos `YRI.samples` y `JPT.samples` con las listas de IDs de individuos de cada población. Para esto necesitamos primero conocer a qué población pertenece cada individuo del Proyecto 1000 Genomas.

Descargue la lista de muestras del servidor ftp de 1000 Genomas:

```bash
wget http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/integrated_call_samples_v3.20130502.ALL.panel
```

(Si hubiere problemas con la conexión, el archivo ya se encuentra descargado en la carpeta del TP. En ese caso, cópielo con: `cp ../integrated_call_samples_v3.20130502.ALL.panel ./`)

Inspeccione el archivo en la línea de comandos:

```bash
column -t integrated_call_samples_v3.20130502.ALL.panel | less
```

¿Qué poblaciones puede observar? Filtre los valores únicos de la segunda columna, "pop" (por "population"), usando la herramienta `uniq`. Para que `uniq` haga lo que esperamos, es necesario ordenar su input:

```bash
awk '{ print $2 }' integrated_call_samples_v3.20130502.ALL.panel | sort | uniq
```

¿Reconoce las poblaciones de 1kG? ¿Cuántas hay en total? (Use `wc -l`).

Además de colapsar líneas iguales, el comando `uniq` nos permite contar (`-c` por "count") el número de ocurrencias de cada valor encontrado. Así pues, podemos utilizarlo para contar la cantidad de muestras por población:

```bash
awk '{ print $2 }' integrated_call_samples_v3.20130502.ALL.panel | sort | uniq -c
```

¿Qué población tiene más muestras? Ordene el resultado con `sort` nuevamente, según el número de muestras:

```bash
awk '{ print $2 }' integrated_call_samples_v3.20130502.ALL.panel | sort | uniq -c | sort -nk1
```

¿Qué población tiene menos muestras?

Para continuar con el ejercicio, nos interesa conservar las muestras cuya población sea YRI, por un lado, y JPT, por el otro. Queremos, además, que el output sea guardado en dos archivos distintos.

Comencemos por filtrar a los YRI:

```bash
grep YRI integrated_call_samples_v3.20130502.ALL.panel
```

¿Se ve bien el filtrado? ¿Cuántas muestras de Yorubas de Ibadán se muestran? Pipee el output de `grep` a `wc -l` para averiguarlo.

Si estamos seguros de que las líneas conservadas son las correctas, procedemos a seleccionar únicamente los IDs de individuos, que están en la primera columna, `$1` para `awk`:

```bash
grep YRI integrated_call_samples_v3.20130502.ALL.panel | awk '{ print $1 }'
```

El output debería mostrar ahora sólo los IDs de individuos. Podemos redirigirlo a un archivo nuevo con el operador `>`:

```bash
grep YRI integrated_call_samples_v3.20130502.ALL.panel | awk '{ print $1 }' > YRI.samples
```

★ PROTIP: El comando `tee` es útil para imprimir en pantalla un resultado y a la vez redirigirlo a un archivo, de modo de chequear el output y almacenarlo al mismo tiempo. Los dos pasos anteriores pueden resumirse en el siguiente:

```bash
grep YRI integrated_call_samples_v3.20130502.ALL.panel | awk '{ print $1 }' | tee YRI.samples
```

Investigue el archivo generado (`YRI.samples`) con `less`.

Sólo falta generar la lista de individuos de la población JPT. Lo haremos de una manera ligeramente distinta, esta vez aprovechando mejor el poder de `awk`:

```bash
awk '$2 == "JPT" { print $1, $2, $4 }' integrated_call_samples_v3.20130502.ALL.panel
```

⚠ Note que `awk` siempre recibe su comando entre comillas simples (`'...'`). Para introducir comillas adentro de la línea de `awk`, debimos recurrir a las comillas dobles (`"..."`), de manera que no termine el comando prematuramente. En nuestro ejemplo: `'... "JPT" ...'`.

El comando anterior imprime los campos 1, 2 y 4 cuando se cumple la condición de que el campo 2 sea igual a “JPT”. Es decir, se queda únicamente con las muestras de japoneses de Tokio. Léalo de nuevo y entienda cómo esta lógica se expresa en la sintaxis de `awk`.

Corra el comando anterior nuevamente, pero esta vez imprima sólo el campo 1 (el ID de las muestras) y escriba el output en otro archivo:

```bash
awk '$2 == "JPT" { print $1 }' integrated_call_samples_v3.20130502.ALL.panel > JPT.samples
```

#### Breve *excursus* sobre `awk`

La sintaxis de `awk` tiene ciertos rincones inhóspitos y, en opinión de este humilde servidor, no vale la pena aprenderla *en profundidad* --hay lenguajes más amigables para parseos complejos, como Python. No obstante, `awk` puede ser muy útil en comandos sencillos como los vistos hasta aquí, que se limitan a la estructura:

```bash
awk 'condición { print algunos campos }' archivo_de_input
```

Lea cada uno de estos comandos detenidamente y a continuación ejecútelo:

```bash
# condición: el campo 1 debe ser igual a “NA19059”:
awk '$1 == "NA19059"' integrated_call_samples_v3.20130502.ALL.panel
# observe que cuando se omite la parte de { print $1, $2 },
# awk imprime toda la línea. de qué población es este individuo?

# misma condición, imprime sólo el campo 2:
awk '$1 == "NA19059" { print $2 }' integrated_call_samples_v3.20130502.ALL.panel

# imprime campos 2 y 3 de todas las líneas, ya que no se especifica condición para filtrarlas. En este caso, son los nombres de población y superpoblación:
awk '{ print $2, $3 }' integrated_call_samples_v3.20130502.ALL.panel 

# imprime la línea cuando $2 es “MXL” y $4 es “female”; es decir, filtra las mujeres mejicanas del proyecto:
awk '$2 == "MXL" && $4 == "female"' integrated_call_samples_v3.20130502.ALL.panel
```

Usando las herramientas que aprendió hasta aquí, responda las siguientes preguntas. No cuente nada a mano! ;)

* ¿Cuántas colombianas de Medellín (población `CLM`) formaron parte del Proyecto 1000 Genomas? 

* ¿Cuántas muestras masculinas de peruanos de Lima (población `PEL`) forman parte del Proyecto?

#### De regreso al TP

Recordemos cuál era nuestro objetivo inicial: tener una lista de los individuos YRI y JPT. Confirmemos que los dos archivos generados previamente con este fin están en su lugar:

```bash
ls -lh *.samples
```

El asterisco (`*`) sirve como "comodín". `*.samples` significa "cualquier archivo que termine en `.samples`". Imprima las primeras líneas de todos los archivos `.samples`:

```bash
head *.samples  # head puede imprimir muchos archivos a la vez
```

¿Cuántos individuos tiene cada grupo y cuántos seleccionamos en total?

```bash
wc -l *.samples
```

Estamos listos para calcular el F<sub>ST</sub> con el comando que antes fallaba:

```bash
vcftools --vcf genotypes.vcf --weir-fst-pop YRI.samples --weir-fst-pop JPT.samples --out YRI_vs_JPT
```

Explore los resultados:

```bash
column -t YRI_vs_JPT.weir.fst | less
```

¿Puede notar que muchas líneas no tienen un valor calculado? (`nan` significa “not a number”). Fíltrelas con `grep -v`:

```bash
column -t YRI_vs_JPT.weir.fst | grep -v 'nan'
```

Ahora quédese con los valores de F<sub>ST</sub> mayores a 0,3. Utilice `awk` para filtrar por los valores de la tercera columna, como aprendió anteriormente. Tenga cuidado: `awk` espera `0.3`, no `0,3`.

¿Cuántos marcadores tienen valores de F<sub>ST</sub> superiores a ese umbral? 

### Graficar en Python con pandas y matplotlib

Como tarea final, graficaremos el F<sub>ST</sub> por posición cromosómica entre las poblaciones YRI y JPT utilizando Python, con las librerías `pandas` (manejo de DataFrames) y `matplotlib` (dibuja gráficos a partir de los datos del DataFrame).

```bash
ipython
```

IPython inicia una sesión interactiva de Python, de modo que el código a continuación pertenece a ese lenguaje y es distinto de los comandos de Bash que forman parte del TP hasta aquí.

Ejecute cada línea de código por separado. Las líneas que comienzan con un numeral (`#`) son “comentarios” del código --partes que no son ejecutadas, de modo que no necesita escribirlas.

```python
# Primero importamos la librería pandas bajo el alias “pd”:
import pandas as pd

# A continuación leemos el archivo con los valores del FST y asignamos
# el DataFrame resultante a una variable “df”:
df = pd.read_table('YRI_vs_JPT.weir.fst')

# Investiguemos las primeras líneas de la tabla:
df.head()

# Ahora ordenamos el DataFrame por los valores de la columna del FST:
df.sort_values(by='WEIR_AND_COCKERHAM_FST', inplace=True)

# Investiguemos las últimas líneas de la tabla:
df.tail()

# ^ Hay demasiados NaNs (not a number, valores que faltan). Quitémoslos:
df.dropna(inplace=True)

# Confirmemos que las últimas líneas ya no tienen NaNs:
df.tail()

# Importemos la API pyplot de la librería matplotlib para graficar:
import matplotlib.pyplot as plt

# Dibujemos un scatter plot con la posición cromosómica en el eje x
# y con el FST en el eje y. Ignore los Gtk-WARNINGs.
df.plot.scatter(x='POS', y='WEIR_AND_COCKERHAM_FST')
plt.show()

# Cierre la ventana del gráfico. Ajustemos la apariencia:
df.plot.scatter(x='POS', y='WEIR_AND_COCKERHAM_FST', s=100, color='DimGrey', lw=0)
plt.show()

# ¿Ve los dos marcadores con valores altos de FST que antes encontramos?
# Cierre el gráfico. Tracemos ahora una línea en el umbral 0,3:
ax = df.plot.scatter(x='POS', y='WEIR_AND_COCKERHAM_FST', s=100, color='DimGrey', lw=0)
ax.axhline(0.3, color='tomato')
plt.show()

exit()
```

### Bonus tracks

Si le queda algo de tiempo, refuerce lo aprendido sobre `awk`, `uniq`, `sort` *et alia* con los siguientes ejercicios. Utilice el archivo de muestras de 1kG, `integrated_call_samples_v3.20130502.ALL.panel` como input. Recuerde que no tiene que tipear el nombre completo cada vez: basta escribir las primeras letras y darle al `Tab`.

**Challenge 1**: ¿Puede filtrar las muestras de mujeres puertorriqueñas (`PUR`) y británicas (`GBR`)? Es decir, genere un comando de `awk` que imprima esas muestras *al mismo tiempo*, utilizando el operador `||` (OR). Pista: puede agrupar condiciones utilizando los paréntesis `(condición1 || condición2) && condición3`.

**Challenge 2**: ¿Cuántas mujeres y cuántos hombres fueron muestreados para 1kG?

**Challenge 3**: Filtre las muestras con IDs que comienzan con "NA" (recuerde a `grep`): ¿Cuántas muestras *por población* cumplen ese requisito? Debería poder responderlo *con una sola línea* de código.

Responda lo mismo para las muestras cuyo ID comienza con "HG". ¿Son otras poblaciones?

**🏆 Challenge 4**: ¿Cuántas poblaciones tiene n100 o  más muestras en 1kG? Recuerde: una sola línea y no vale contar a mano.

**🏆 Challenge 5**: ¿Puede contar cuántas poblaciones hay por cada superpoblación? Quédese con las columnas relevantes y luego utilice lo que aprendió del comando `uniq -c`. Pista inicial: comience imprimiendo sólo las columnas de población y superpoblación. El objetivo final es obtener:

```
7 AFR
4 AMR
5 EAS
...
```

